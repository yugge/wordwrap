//
// Wordwraping for go. v 0.1
// 2013 - Yugge
//

package wordwrap

import "strings"

func Wrap(text string, width int, excludedChars string) (out []string) {
	var lastCursorPosition = 0
	for cursor := width; cursor < len(text); cursor += width {
		for {
			if strings.Contains(text[lastCursorPosition:cursor+1], " ") != true {
				out = append(out, text[lastCursorPosition:cursor])
				break
			}

			if strings.ContainsAny(text[cursor:cursor+1], excludedChars) {
				out = append(out, text[lastCursorPosition:cursor])
				break
			}

			if text[cursor] == ' ' {
				out = append(out, text[lastCursorPosition:cursor])
				cursor += 1
				break
			} else {
				if cursor != 0 {
					cursor -= 1
					continue
				}
				break
			}
		}
		lastCursorPosition = cursor
	}
	out = append(out, text[lastCursorPosition:])
	return
}
